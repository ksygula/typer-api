package pwsip.projektzespolowy.projektzespolowyapi.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.*;
import pwsip.projektzespolowy.projektzespolowyapi.Entity.ApiResponseEntity;
import pwsip.projektzespolowy.projektzespolowyapi.Entity.RoundEntity;
import pwsip.projektzespolowy.projektzespolowyapi.Service.AllSportsApiService;

@RestController
@RequestMapping("/allSportsApi")
public class AllSportsApiController {

    @Autowired
    AllSportsApiService allSportsApiService;

    @Scheduled(cron = "0 30 22 * * ?")
    @GetMapping("/getfixtures")
    public String getApiResponseEntity(){
        ApiResponseEntity apiResponse = allSportsApiService.getFixtures();
        allSportsApiService.saveFixtures(apiResponse);
        return apiResponse.toString();
    }

    @RequestMapping(method = RequestMethod.POST, value = "/add")
    public void addRound(@RequestBody RoundEntity round){
        allSportsApiService.addRound(round);
    }

}
