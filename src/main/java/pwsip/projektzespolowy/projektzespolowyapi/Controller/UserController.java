package pwsip.projektzespolowy.projektzespolowyapi.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import pwsip.projektzespolowy.projektzespolowyapi.Entity.ApiResponse;
import pwsip.projektzespolowy.projektzespolowyapi.Entity.Auth.UserDto;
import pwsip.projektzespolowy.projektzespolowyapi.Entity.Auth.UserEntity;
import pwsip.projektzespolowy.projektzespolowyapi.Service.UserService;

import java.util.List;

@RestController
@RequestMapping("/users")
@CrossOrigin(value = "http://localhost:4200", maxAge = 3600)
public class UserController {

    @Autowired
    private UserService userService;

    @RequestMapping(value="/signup", method = RequestMethod.POST)
    public ApiResponse<UserEntity> saveUser(@RequestBody UserDto user){
        return new ApiResponse<>(HttpStatus.OK.value(), "User saved successfully.",userService.save(user));
    }

    @GetMapping
    public ApiResponse<List<UserEntity>> listUser(){
        return new ApiResponse<>(HttpStatus.OK.value(), "User list fetched successfully.",userService.findAll());
    }

    @GetMapping("/{id}")
    public ApiResponse<UserEntity> getOne(@PathVariable Integer id){
        return new ApiResponse<>(HttpStatus.OK.value(), "User fetched successfully.",userService.findById(id));
    }

    @GetMapping("/points/{username}")
    public Integer getUsersPoints(@PathVariable String username){
        userService.saveUserIfNotFound(username);
        return userService.getUserPoints(username);
    }

    @PutMapping("/{id}")
    public ApiResponse<UserDto> update(@RequestBody UserDto userDto) {
        return new ApiResponse<>(HttpStatus.OK.value(), "User updated successfully.",userService.update(userDto));
    }

    @DeleteMapping("/{id}")
    public ApiResponse<Void> delete(@PathVariable Integer id) {
        userService.delete(id);
        return new ApiResponse<>(HttpStatus.OK.value(), "User deleted successfully.", null);
    }
}