package pwsip.projektzespolowy.projektzespolowyapi.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import pwsip.projektzespolowy.projektzespolowyapi.Entity.ApiResponse;
import pwsip.projektzespolowy.projektzespolowyapi.Entity.Prediction.PredictionDto;
import pwsip.projektzespolowy.projektzespolowyapi.Entity.Prediction.PredictionEntity;
import pwsip.projektzespolowy.projektzespolowyapi.Service.PredictionService;

@RestController
@RequestMapping("/prediction")
@CrossOrigin(value = "http://localhost:4200", maxAge = 3600)
public class PredictionController {

    @Autowired
    PredictionService predictionService;

    @RequestMapping(value="/add", method = RequestMethod.POST)
    public ApiResponse<PredictionEntity> savePrediction(@RequestBody PredictionDto prediction){
        return new ApiResponse<>(HttpStatus.OK.value(), "Prediction saved successfully.",predictionService.add(prediction));
    }
}
