package pwsip.projektzespolowy.projektzespolowyapi.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pwsip.projektzespolowy.projektzespolowyapi.Entity.FixturesEntity;
import pwsip.projektzespolowy.projektzespolowyapi.Entity.RoundEntity;
import pwsip.projektzespolowy.projektzespolowyapi.Repository.FixturesEntityRepository;
import pwsip.projektzespolowy.projektzespolowyapi.Service.FixturesService;

import java.util.List;

@RestController
@RequestMapping("/api")
public class FixturesController {

    @Autowired
    FixturesEntityRepository fixturesEntityRepository;
    @Autowired
    FixturesService fixturesService;

    @RequestMapping("/fixtures")
    @CrossOrigin("http://localhost:4200")
    public List<FixturesEntity> getFixtures(){
        return fixturesService.getFixturesList();
    }

    @RequestMapping("/round")
    @CrossOrigin("http://localhost:4200")
    public RoundEntity getCurrentRound(){
        return fixturesService.getCurrentRound();
    }
}
