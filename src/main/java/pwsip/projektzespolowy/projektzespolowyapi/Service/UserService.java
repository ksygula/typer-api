package pwsip.projektzespolowy.projektzespolowyapi.Service;

import org.keycloak.representations.AccessToken;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pwsip.projektzespolowy.projektzespolowyapi.Entity.Auth.UserDto;
import pwsip.projektzespolowy.projektzespolowyapi.Entity.Auth.UserEntity;
import pwsip.projektzespolowy.projektzespolowyapi.Repository.UserRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class UserService {

    @Autowired
    UserRepository userRepository;
    @Autowired
    private AccessToken accessToken;

    public List<UserEntity> findAll() {
        List<UserEntity> list = new ArrayList<>();
        userRepository.findAll().iterator().forEachRemaining(list::add);
        return list;
    }

    public void delete(Integer id) {
        userRepository.deleteById(id);
    }

    public UserEntity findOne(String username) {
        return userRepository.findUserEntityByUsername(username);
    }

    public UserEntity findById(Integer id) {
        Optional<UserEntity> optionalUser = userRepository.findById(id);
        return optionalUser.isPresent() ? optionalUser.get() : null;
    }

    public UserDto update(UserDto userDto) {
        UserEntity user = findById(userDto.getId());
        if(user != null) {
            BeanUtils.copyProperties(userDto, user, "points");
            userRepository.save(user);
        }
        return userDto;
    }

    public UserEntity save(UserDto user) {
        UserEntity newUser = new UserEntity();
        newUser.setUsername(user.getUsername());
        newUser.setFirstName(user.getFirstName());
        newUser.setLastName(user.getLastName());
        newUser.setEmail(user.getEmail());
        newUser.setPoints(0);
        return userRepository.save(newUser);
    }

    public Integer getUserPoints(String username) {
        UserEntity user = userRepository.findUserEntityByUsername(username);
        return user.getPoints();
    }

    public void saveUserIfNotFound(String username) {
        UserEntity user = userRepository.findUserEntityByUsername(username);
        if(user == null) {
            UserEntity newUser = new UserEntity();
            newUser.setEmail(accessToken.getEmail());
            newUser.setFirstName(accessToken.getGivenName());
            newUser.setLastName(accessToken.getFamilyName());
            newUser.setUsername(accessToken.getPreferredUsername());
            newUser.setPoints(0);
            userRepository.save(newUser);
        }
    }
}
