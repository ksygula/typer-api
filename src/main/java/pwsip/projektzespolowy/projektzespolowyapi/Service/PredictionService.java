package pwsip.projektzespolowy.projektzespolowyapi.Service;

import org.keycloak.representations.AccessToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pwsip.projektzespolowy.projektzespolowyapi.Entity.Auth.UserEntity;
import pwsip.projektzespolowy.projektzespolowyapi.Entity.Prediction.PredictionDto;
import pwsip.projektzespolowy.projektzespolowyapi.Entity.Prediction.PredictionEntity;
import pwsip.projektzespolowy.projektzespolowyapi.Repository.PredictionRepository;
import pwsip.projektzespolowy.projektzespolowyapi.Repository.UserRepository;

import java.time.LocalDateTime;

@Service
public class PredictionService {

    @Autowired
    PredictionRepository predictionRepository;
    @Autowired
    UserRepository userRepository;
    @Autowired
    private AccessToken accessToken;

    public PredictionEntity add(PredictionDto predictionDto) {
        UserEntity userEntity = userRepository.findUserEntityByUsername(accessToken.getPreferredUsername());

        PredictionEntity prediction = new PredictionEntity();
        prediction.setEventKey(predictionDto.getEventKey());
        prediction.setHomeTeamScore(predictionDto.getHomeTeamScore());
        prediction.setAwayTeamScore(predictionDto.getAwayTeamScore());
        prediction.setAddDate(LocalDateTime.now());
        prediction.setUserEntity(userEntity);
        return predictionRepository.save(prediction);
    }
}
