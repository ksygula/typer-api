package pwsip.projektzespolowy.projektzespolowyapi.Service;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import pwsip.projektzespolowy.projektzespolowyapi.Entity.ApiResponseEntity;
import pwsip.projektzespolowy.projektzespolowyapi.Entity.ApiResponseFixturesEntity;
import pwsip.projektzespolowy.projektzespolowyapi.Entity.FixturesEntity;
import pwsip.projektzespolowy.projektzespolowyapi.Entity.RoundEntity;
import pwsip.projektzespolowy.projektzespolowyapi.Repository.FixturesEntityRepository;
import pwsip.projektzespolowy.projektzespolowyapi.Repository.RoundRepository;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Service
public class AllSportsApiService {

    @Autowired
    RestTemplate restTemplate;
    @Autowired
    RoundRepository roundRepository;
    @Autowired
    FixturesEntityRepository fixturesEntityRepository;
    @Autowired
    ModelMapper modelMapper;

    @Value("${allSportsApi.fixturesURL}")
    String fixturesURL;
    @Value("${allSportsApi.APIkey}")
    String ApiKey;

    public ApiResponseEntity getFixtures(){
        RoundEntity roundEntity = roundRepository.findTopByOrderByIdDesc();

        if(LocalDate.now().isAfter(roundEntity.getDateTo())){
            RoundEntity newRound = new RoundEntity();
            newRound.setDateFrom(roundEntity.getDateTo().plusDays(1));
            newRound.setDateTo(roundEntity.getDateTo().plusDays(7));
            roundRepository.save(newRound);
            return restTemplate.getForObject(fixturesURL, ApiResponseEntity.class, ApiKey, newRound.getDateFrom(), newRound.getDateTo());
        } else return restTemplate.getForObject(fixturesURL, ApiResponseEntity.class, ApiKey, roundEntity.getDateFrom(), roundEntity.getDateTo());
    }

    public void addRound(RoundEntity round) {
        roundRepository.save(round);
    }

    public void saveFixtures(ApiResponseEntity apiResponseEntity){
        RoundEntity currentRound = roundRepository.findTopByOrderByIdDesc();

        for(ApiResponseFixturesEntity responseFixture : apiResponseEntity.fixtures){
            FixturesEntity fixture = modelMapper.map(responseFixture, FixturesEntity.class);
            fixture.setRoundEntity(currentRound);
            fixturesEntityRepository.save(fixture);
        }
    }
}
