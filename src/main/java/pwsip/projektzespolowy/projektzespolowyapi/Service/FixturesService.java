package pwsip.projektzespolowy.projektzespolowyapi.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pwsip.projektzespolowy.projektzespolowyapi.Entity.FixturesEntity;
import pwsip.projektzespolowy.projektzespolowyapi.Entity.RoundEntity;
import pwsip.projektzespolowy.projektzespolowyapi.Repository.FixturesEntityRepository;
import pwsip.projektzespolowy.projektzespolowyapi.Repository.RoundRepository;

import java.util.List;

@Service
public class FixturesService {

    @Autowired
    FixturesEntityRepository fixturesEntityRepository;
    @Autowired
    RoundRepository roundRepository;

//    public List<FixturesEntity> getFixturesList(){
//        return (List<FixturesEntity>) fixturesEntityRepository.findAll();
//    }

    public List<FixturesEntity> getFixturesList(){
        return fixturesEntityRepository.findAllByRoundEntity(getCurrentRound());
    }

    public RoundEntity getCurrentRound() {
        return roundRepository.findTopByOrderByIdDesc();
    }
}
