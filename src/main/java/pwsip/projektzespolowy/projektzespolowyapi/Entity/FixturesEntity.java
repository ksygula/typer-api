package pwsip.projektzespolowy.projektzespolowyapi.Entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = "Fixtures")
public class FixturesEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(nullable = false)
    private Long id;
    private String eventKey;
    private String eventDate;
    private String eventTime;
    private String eventHomeTeam;
    private String eventAwayTeam;
    private String eventFinalResult;
    private String eventStatus;
    private String countryName;
    private String leagueName;
    private String leagueRound;
    @ManyToOne
    @JoinColumn(name = "round_id")
    @JsonBackReference
    private RoundEntity roundEntity;
}
