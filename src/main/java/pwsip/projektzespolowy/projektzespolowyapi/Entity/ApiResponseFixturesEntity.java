package pwsip.projektzespolowy.projektzespolowyapi.Entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class ApiResponseFixturesEntity {
    @JsonProperty("event_key")
    public String eventKey;
    @JsonProperty("event_date")
    public String eventDate;
    @JsonProperty("event_time")
    public String eventTime;
    @JsonProperty("event_home_team")
    public String eventHomeTeam;
    @JsonProperty("event_away_team")
    public String eventAwayTeam;
    @JsonProperty("event_final_result")
    public String eventFinalResult;
    @JsonProperty("event_status")
    public String eventStatus;
    @JsonProperty("country_name")
    public String countryName;
    @JsonProperty("league_name")
    public String leagueName;
    @JsonProperty("league_round")
    public String leagueRound;


}
