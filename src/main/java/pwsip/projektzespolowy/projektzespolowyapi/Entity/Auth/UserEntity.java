package pwsip.projektzespolowy.projektzespolowyapi.Entity.Auth;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Getter;
import lombok.Setter;
import pwsip.projektzespolowy.projektzespolowyapi.Entity.Prediction.PredictionEntity;

import javax.persistence.*;
import java.util.Set;

@Getter
@Setter
@Entity
@Table(name = "Users")
public class UserEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(nullable = false)
    private Integer id;
    private String firstName;
    private String lastName;
    private String username;
    private String email;
    private Integer points;

    @OneToMany(mappedBy = "userEntity", cascade = CascadeType.ALL)
    @JsonManagedReference
    private Set<PredictionEntity> predictions;
}
