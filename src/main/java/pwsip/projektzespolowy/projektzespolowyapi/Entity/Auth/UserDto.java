package pwsip.projektzespolowy.projektzespolowyapi.Entity.Auth;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserDto {
    private Integer id;
    private String firstName;
    private String lastName;
    private String username;
    private String email;
    private Integer points;
}
