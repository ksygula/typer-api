package pwsip.projektzespolowy.projektzespolowyapi.Entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class ApiResponseEntity {

    @JsonProperty("success")
    public Integer success;
    @JsonProperty("result")
    public List<ApiResponseFixturesEntity> fixtures = null;
}