package pwsip.projektzespolowy.projektzespolowyapi.Entity;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;


@Entity
@Table(name = "Round")
@Getter
@Setter
public class RoundEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(nullable = false)
    private Long id;
    private LocalDate dateFrom;
    private LocalDate dateTo;
    private Boolean accounted;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "roundEntity")
    @JsonManagedReference
    private List<FixturesEntity> fixturesEntityList;
}
