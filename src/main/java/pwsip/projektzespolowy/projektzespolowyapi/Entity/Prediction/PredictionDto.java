package pwsip.projektzespolowy.projektzespolowyapi.Entity.Prediction;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PredictionDto {
    private String eventKey;
    private Integer homeTeamScore;
    private Integer awayTeamScore;
}
