package pwsip.projektzespolowy.projektzespolowyapi.Entity.Prediction;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Getter;
import lombok.Setter;
import pwsip.projektzespolowy.projektzespolowyapi.Entity.Auth.UserEntity;

import javax.persistence.*;
import java.time.LocalDateTime;

@Getter
@Setter
@Entity
@Table(name = "Predictions")
public class PredictionEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(nullable = false)
    private Long id;
    private String eventKey;
    private Integer homeTeamScore;
    private Integer awayTeamScore;
    private LocalDateTime addDate;
    @ManyToOne
    @JoinColumn
    @JsonBackReference
    private UserEntity userEntity;
}
