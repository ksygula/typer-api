package pwsip.projektzespolowy.projektzespolowyapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class ProjektZespolowyApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(ProjektZespolowyApiApplication.class, args);
    }

}
