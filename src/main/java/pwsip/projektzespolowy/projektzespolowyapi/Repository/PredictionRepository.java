package pwsip.projektzespolowy.projektzespolowyapi.Repository;

import org.springframework.data.repository.CrudRepository;
import pwsip.projektzespolowy.projektzespolowyapi.Entity.Prediction.PredictionEntity;

public interface PredictionRepository extends CrudRepository<PredictionEntity, Long> {
}
