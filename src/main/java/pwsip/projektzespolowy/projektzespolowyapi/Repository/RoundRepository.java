package pwsip.projektzespolowy.projektzespolowyapi.Repository;

import org.springframework.data.repository.CrudRepository;
import pwsip.projektzespolowy.projektzespolowyapi.Entity.RoundEntity;

public interface RoundRepository extends CrudRepository<RoundEntity, Long> {

    RoundEntity findTopByOrderByIdDesc();
}

