package pwsip.projektzespolowy.projektzespolowyapi.Repository;

import org.springframework.data.repository.CrudRepository;
import pwsip.projektzespolowy.projektzespolowyapi.Entity.Auth.UserEntity;

public interface UserRepository extends CrudRepository<UserEntity, Integer> {
    UserEntity findUserEntityByUsername(String username);
}
