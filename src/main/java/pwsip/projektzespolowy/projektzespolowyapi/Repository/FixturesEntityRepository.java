package pwsip.projektzespolowy.projektzespolowyapi.Repository;

import org.springframework.data.repository.CrudRepository;
import pwsip.projektzespolowy.projektzespolowyapi.Entity.FixturesEntity;
import pwsip.projektzespolowy.projektzespolowyapi.Entity.RoundEntity;

import java.util.List;

public interface FixturesEntityRepository extends CrudRepository<FixturesEntity, Long> {
    List<FixturesEntity> findAllByRoundEntity(RoundEntity roundEntity);
}
